
import fibonacci as fib


##
## Main program
## Python uses the name to indicate the main program. If the program is executed, the code below is called.
##

if __name__ == '__main__':

    # get 10 Fibonacci numbers
    Fib = fib.compute_Fibonacci(n_numbers=10)

    # write the Fibonacci numbers to a file
    with open('fibonacci.txt','w') as f:
        for number in Fib:
            f.write(str(number) + '\n')

    
    

