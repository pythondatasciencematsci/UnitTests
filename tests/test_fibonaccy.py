from unittestdemo import fibonacci as fib
import pytest


def test_fibonacci_sequence():
    numbers = fib.compute_Fibonacci(n_numbers=10)
    assert numbers == [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]


def test_raise_TypeError():
    with pytest.raises(TypeError):
        numbers = fib.compute_Fibonacci(n_numbers="10")


# add a test for integers n_numbers < 2
#
#  ... your code here ...
# Hint:
# - modify compute_Fibonacci to catch these cases
# - add a corresponding test
#
